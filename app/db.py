import traceback
from datetime import date, datetime

import pymysql.cursors


USER = "projectman"
PASSWORD = "pass"
DB = "project"


def connect():
    """Creates a new connection to the sql database"""
    return pymysql.connect(host='localhost',
                           user=USER,
                           password=PASSWORD,
                           db=DB,
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)


def _sql_wrapper(query, values=(), retrieve=False):
    """Silently creates a connection"""
    db = connect()
    cursor = db.cursor()
    result = None
    try:
        if (values):
            cursor.execute(query, values)
        else:
            cursor.execute(query)

        if retrieve:
            result = cursor.fetchall()

        db.commit()

    except Exception:
        print(traceback.format_exc())
        db.rollback()
    finally:
        db.close()
    return result


def create_db():
    """Attempts to create the tables if they don't already exist"""

    db = connect()
    cursor = db.cursor()

    try:
        cursor.execute('''
        CREATE TABLE IF NOT EXISTS Player(
        PID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(20)
        )
        ''')

        cursor.execute('''
        CREATE TABLE IF NOT EXISTS Game(
        GID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        Game_Date DATE,
        winner INTEGER,
        FOREIGN KEY (winner) REFERENCES Player(PID)
        )
        ''')

        cursor.execute('''

        CREATE TABLE IF NOT EXISTS Duel(
        GID INTEGER NOT NULL PRIMARY KEY,
        Opponent INTEGER,
        FOREIGN KEY (GID) REFERENCES Game(GID) ON DELETE CASCADE,
        FOREIGN KEY (Opponent) REFERENCES Player(PID)
        )

        ''')

        cursor.execute('''
        CREATE TABLE IF NOT EXISTS FFA(
        GID INTEGER NOT NULL PRIMARY KEY,
        Player2 INTEGER,
        Player3 INTEGER,
        Player4 INTEGER,
        FOREIGN KEY (GID) REFERENCES Game(GID) ON DELETE CASCADE,
        FOREIGN KEY (Player2) REFERENCES Player(PID),
        FOREIGN KEY (Player3) REFERENCES Player(PID),
        FOREIGN KEY (Player4) REFERENCES Player(PID)
        )
        ''')

        cursor.execute('''
        CREATE TABLE IF NOT EXISTS Deck(
        DID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(20),
        archetype VARCHAR(20)
        )
        ''')

        cursor.execute('''
        CREATE TABLE IF NOT EXISTS Build(
        BID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
        Decklist VARCHAR(20),
        BasedOn INTEGER,
        FOREIGN KEY (BasedOn) References Build(BID)
        )
        ''')

        cursor.execute('''
        CREATE TABLE IF NOT EXISTS PlayedIn(
        PID INTEGER NOT NULL,
        GID INTEGER NOT NULL,
        DID INTEGER NOT NULL,
        FOREIGN KEY (PID) REFERENCES Player(PID) ON DELETE CASCADE,
        FOREIGN KEY (GID) REFERENCES Game(GID) ON DELETE CASCADE,
        FOREIGN KEY (DID) REFERENCES Deck(DID) ON DELETE CASCADE,
        PRIMARY KEY (PID, GID, DID)
        )
        ''')

        db.commit()

    except Exception:
        print("Exception occurred, rolling back")
        print(traceback.format_exc())
        db.rollback()
    finally:
        db.close()



def create_or_update_user(username, pid=None):
    """Creates a user if a user doesnt already exist with a passed id. Returns whether the query succeeded or the new user PID"""

    if pid:
        if _sql_wrapper("SELECT * FROM Player WHERE PID=%s", str(pid), retrieve=True):
            return pid

        _sql_wrapper("INSERT INTO `Player` (`name`, `PID`) VALUES (%s, %s) ", (username, pid))

    else:
        last = None
        try:
            db = connect()
            cursor = db.cursor()
            cursor.execute("INSERT INTO `Player` (`name`) VALUES (%s)", username)
            last = cursor.lastrowid
            db.commit()
        except pymysql.err.Error:
            return False
        finally:
            db.close()
        return last

    return True


def create_or_update_deck(deckname, archetype, did=None):
    """Creates a deck if a deck doesnt already exist with a passed id. Returns whether the query succeeded or the new deck PID"""


    if did:

        if _sql_wrapper("SELECT * FROM Deck WHERE DID=%s", str(did), retrieve=True):
            return did

        _sql_wrapper("INSERT INTO `Player` (`name`, `archetype`, `DID`) VALUES (%s, %s) ", (deckname, archetype, did))
    else:
        last = None
        try:
            db = connect()
            cursor = db.cursor()
            cursor.execute("INSERT INTO `Deck` (`name`, `archetype`) VALUES (%s, %s)", (deckname, archetype))
            last = cursor.lastrowid
            db.commit()
        except pymysql.err.Error:
            return False
        finally:
            db.close()
        return last

    return True



def apropos_user(name):
    """Find a user with a name LIKE name"""
    return _sql_wrapper("SELECT * FROM Player WHERE name LIKE %s", ('%' + name + '%'), retrieve=True)

def apropos_deck(name):
    """Find a deck with a name LIKE name"""
    return _sql_wrapper("SELECT * FROM Player WHERE name LIKE %s", ('%' + name + '%'), retrieve=True)


def create_game(winner, today=None):

    if today is None:
        today = datetime.now().date()

    last = None
    try:
        db = connect()
        cursor = db.cursor()
        cursor.execute("INSERT INTO `Game` (`winner`, `Game_Date`) VALUES (%s, %s)", (winner, today))
        last = cursor.lastrowid
        db.commit()
    except pymysql.err.Error:
        return None
    finally:
        db.close()

    return last


def playedin(Game, Player, deck):
    _sql_wrapper("INSERT INTO `PlayedIn` (`GID`, `PID`, `DID`) VALUES (%s, %s, %s)", (Game, Player, deck))


def add_opponent(Gameid, opponent_id):
    _sql_wrapper("INSERT INTO `Duel` (`GID`, `Opponent`) VALUES (%s, %s)", (Gameid, opponent_id))


def get_top_matches():

    return _sql_wrapper('''
    SELECT Game.GID, Player.name, Deck.name, Game.Game_Date FROM Player, PlayedIn, Deck, Game WHERE
    PlayedIn.PID = Player.PID
    AND PlayedIn.DID = Deck.DID
    AND PlayedIn.GID = Game.GID limit 15
    ''', retrieve=True)


def get_wins_for(pid):
    return _sql_wrapper('''
    SELECT Player.name, COUNT(*) FROM Game, Player WHERE Player.PID = Game.Winner GROUP BY PID;
    ''', retrieve=True)
