import os
import db

from flask import Flask
from flask import render_template, request, flash, url_for, redirect
app = Flask(__name__)

try:
    app.secret_key = os.environ["FLASK_SECRET"]
except KeyError:
    app.secret_key = "testing_key"

app.config['SESSION_TYPE'] = 'memcached'


db.create_db()

@app.route('/')
@app.route('/index')
@app.route('/index.html')
def index():
    """Home page"""
    return render_template('index.html')


@app.route('/top/')
def top():
    """Last 10 games"""
    result = db.get_top_matches()
    return render_template("top.html", games=result)


@app.route('/input/', methods=['GET', 'POST'])
def deck_input():
    """Build new game entry from user input"""
    # FFA Game
    if request.method == 'POST':
        handle_deck_input(request.form)
        flash("Success")
        return render_template("input.html")

    return render_template("input.html")


def handle_deck_input(form):
    """Creates or updates the decks and players"""

    players = []
    decks = []

    # If the id is pre determined by the user
    if len(form["winner"].split("#")) == 2:
        name, pid = form["winner"].split("#")
        db.create_or_update_user(name, pid)
        players.append(pid)
    else:
        name = form["winner"]
        pid = db.create_or_update_user(name)
        players.append(pid)

    windeck = form["windeck"]
    winarchetype = form["winarchetype"]
    windid = db.create_or_update_deck(windeck, winarchetype)
    decks.append(windid)

    gameid = db.create_game(pid)

    #If this is FFA
    if form["player3"]:
        for i in range(2, 5):

            name = None
            pid = None

            user = "player" + str(i)

            if len(form[user].split("#")) == 2:
                name, pid = form[user].split("#")
                db.create_or_update_user(name, pid)
            else:
                name = form[user]
                pid = db.create_or_update_user(name)

            players.append(pid)

            deck = "deck" + str(i)
            archetype = "archetype" + str(i)

            did = db.create_or_update_deck(form[deck], form[archetype])
            db.playedin(gameid, pid, did)

    else:
        user = "player2"
        name = None
        pid = None
        if len(form["player2"].split("#")) == 2:
            name, pid = form[user].split("#")
            db.create_or_update_user(name, pid)
        else:
            pid = db.create_or_update_user(name, pid)

        did = db.create_or_update_deck(form["deck2"], form["archetype2"])
        db.playedin(gameid, pid, did)


@app.route('/player/', methods=["GET", "POST"])
def player_search():
    """Search for a player by name"""
    if request.method == 'POST':
        return redirect("/player/" + request.form["name"])
    return render_template("find.html", entity="Player")


@app.route('/player/<player>')
def player_info(player):
    """Get info for the player passed in the url"""
    data = db.apropos_user(player)
    return render_template("player.html", players=data)


@app.route('/deck/', methods=["GET", "POST"])
def deck_search():
    if request.method == 'POST':
        return redirect("/deck/" + request.form["name"])
    return render_template("find.html", entity="Deck")


@app.route('/deck/<deck>')
def deck_info(deck):
    data = db.apropos_deck(deck)
    return render_template("deck.html", decks=data)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404


if __name__ == "__main__":
    app.run(host="0.0.0.0")
